from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    model = models.CharField(max_length=50)

    def __str__(self):
        return self.vin


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField(max_length=400)
    phone_number = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    id = models.IntegerField(primary_key=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_employee_sales_list", kwargs={"pk": self.pk})

class SalesRecord(models.Model):
    price = models.IntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="customer",
        on_delete=models.PROTECT
    )
