import React, { useEffect, useState } from "react";

function SalesPersonHistory() {
    const [salesPersons, setSalesPersons] = useState([]);
    const [sales, setSales] = useState([]);
    const [selectedSalesPerson, setSelectedSalesPerson] = useState('');

    const handleSelectedSalesPersonChange = (event) => {
      setSelectedSalesPerson(event.target.value);
    };

    const fetchSalesPersons = async () => {
      const url = "http://localhost:8090/api/sales/employee/";
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setSalesPersons(data.sales_person);
      }
    };

    const fetchSales = async () => {
      const url = "http://localhost:8090/api/sales/";
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      }
    };

    useEffect(() => {
      fetchSalesPersons();
      fetchSales();
    }, []);

    return (
      <>
        <h2 className="gap-3 p-2 mt-3">Sales Person History</h2>
        <div className="mb-3">
          <select onChange={handleSelectedSalesPersonChange} value={selectedSalesPerson} id="sales_person" name="sales_person" className="form-select">
            <option value="">Choose a Sales Person</option>
            {salesPersons.map((salesPerson) => {
              return (
                <option key={salesPerson.name} value={salesPerson.name}>
                  {salesPerson.name}
                </option>
              );
            })}
          </select>
        </div>
        <table className="table table-striped shadow p-4 mt-4">
          <thead>
            <tr>
              <th scope="col">Sales Person</th>
              <th scope="col">Customer</th>
              <th scope="col">VIN</th>
              <th scope="col">Sale Price</th>
            </tr>
          </thead>
          <tbody>

            {sales.filter((sale) => {
              return selectedSalesPerson === '' ? sale : sale.sales_person.name.includes(selectedSalesPerson)
            }).map(sale => {
              return(
                <tr key={sale.id}>
                <td>{sale.sales_person.name}</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
              )
            })}
          </tbody>
        </table>
      </>
    );
  }

  export default SalesPersonHistory;
