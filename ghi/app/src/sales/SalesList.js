import React, { useEffect, useState } from "react";

function SalesRecordList() {

    const[sales, setSales] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    return (
      <>
        <h1 className="gap-3 p-2 mt-3">Sales Record</h1>
        <table className="table table-striped shadow p-4 mt-4">
        <thead>
          <tr>
            <th scope="col">Sales Person</th>
            <th scope="col">Employee ID</th>
            <th scope="col">Customer</th>
            <th scope="col">VIN</th>
            <th scope="col">Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{ sale.sales_person.name }</td>
                <td>{ sale.sales_person.id }</td>
                <td>{ sale.customer.name }</td>
                <td>{ sale.automobile.vin }</td>
                <td>{ sale.price }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    )
}

export default SalesRecordList
