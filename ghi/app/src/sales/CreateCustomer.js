import React, { useState } from 'react';

function CustomerForm() {
    const [customerName, setCustomerName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');


    const handleNameChange = (event) => {
        setCustomerName(event.target.value);
    }
    const handleAddressChange = (event) => {
        setAddress(event.target.value);
    }
    const handlePhoneNumberChange = (event) => {
        setPhoneNumber(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = customerName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerUrl = 'http://localhost:8090/api/sales/customer/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();

            setCustomerName('');
            setAddress('');
            setPhoneNumber('');
        }
    }
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Customer</h1>
              <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} value={customerName} placeholder="Customer name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                  <label forhtml="customer_name">Customer Name</label>
                </div>
                <div className="mb-3">
                  <label forhtml="synopsis">Address</label>
                  <textarea onChange={handleAddressChange} value={address} className="form-control" id="address" rows="3" name="address"></textarea>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePhoneNumberChange} value={phoneNumber} placeholder="Customer Phone Number" type="text" name="phone_number" id="phone_number" className="form-control" />
                  <label forhtml="company_name">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}


export default CustomerForm
