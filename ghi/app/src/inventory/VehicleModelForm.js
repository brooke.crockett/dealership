import React, { useEffect, useState } from 'react';

function VehicleModelForm() {

    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [picture, setPicture] = useState('');

    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    }
    const handleNameChange = (event) => {
        setName(event.target.value);
    }
    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    }

    const fetchData = async () => {
        const url = `http://localhost:8100/api/manufacturers/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = picture;
        data.manufacturer_id = manufacturer;

        const vehicleUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(vehicleUrl, fetchConfig);
        if (response.ok) {
            const newVehicleModel = await response.json();

            setName('');
            setPicture('');
            setManufacturer('');
        }
    }
    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Vehicle Model</h1>
                        <form onSubmit={handleSubmit} id="create-vehicle-model-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="vehicle" id="vehicle" className="form-control" />
                                <label forhtml="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureChange} value={picture} placeholder="Picture URL" required type="text" name="picture" id="picture" className="form-control" />
                                <label forhtml="name">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleManufacturerChange} value={manufacturer} required id="manufacturer" name="manufacturer" className="form-select">
                                    <option value="">Choose a Manufacturer</option>
                                    {manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div >
        </div >
    )
}
export default VehicleModelForm
