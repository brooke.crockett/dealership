import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to={"/"}>CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to={"/"}>Home</NavLink>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href={"#"} role="button" data-bs-toggle="dropdown" aria-expanded="false">Inventory</a>
              <ul className='dropdown-menu'>
              <li>
                  <Link className="dropdown-item" to="manufacturers">Manufacturers</Link>
                  <Link className="dropdown-item" to="manufacturers/create">Add Manufacturers</Link>
                  <Link className="dropdown-item" to="vehicle">Models</Link>
                  <Link className="dropdown-item" to="vehicle/create">Add Models</Link>
                  <Link className="dropdown-item" to="inventory">Inventory</Link>
                  <Link className="dropdown-item" to="inventory/create">Add to Inventory</Link>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Service</a>
              <ul className='dropdown-menu'>
              <li>
                  <Link className="dropdown-item" to="/technicians">Technicians</Link>
                  <Link className="dropdown-item" to="/technicians/new">Add New Technician</Link>
                  <Link className="dropdown-item" to="/appointments">Appointments</Link>
                  <Link className="dropdown-item" to="/appointments/new">Add New Appointment</Link>
                </li>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
              <ul className='dropdown-menu'>
              <li>
                  <Link className="dropdown-item" to="sales">Sales Record</Link>
                  <Link className="dropdown-item" to="sales/history">Filter Sales Record</Link>
                  <Link className="dropdown-item" to="sales/create">Add New Sales</Link>
                  <Link className="dropdown-item" to="salesperson">Add Sales Person</Link>
                  <Link className="dropdown-item" to="customer">Add New Customer</Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
